var xSize = process.argv[2] || 8;
var ySize = process.argv[3] || 8;

for (let i = 0; i < xSize; i++) {
    let row = getSymbol(i);
    for (let j = 1; j < ySize; j++ ) {
        row+=getSymbol(i+j);
    }
    console.log(row);
}

function getSymbol(cell) {
    let result;
    if (cell % 2) {
        result = "#";
    } else {
        result = " ";
    }
    return result;
}

