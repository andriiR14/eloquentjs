for (let i = 0; i < 8; i++) {
    let row = getSymbol(i);
    for (let j = 1; j < 8; j++ ) {
        row+=getSymbol(i+j);
    }
    console.log(row);
}

function getSymbol(cell) {
    let result;
    if (cell % 2) {
        result = "#";
    } else {
        result = " ";
    }
    return result;
}