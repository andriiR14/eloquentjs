for (let i = 1; i <= 100; i++) {
    let print;
    if (i%3 == 0) {
        print = "Fizz";
    } 
    if (i%5 == 0) {
        if (print == undefined) {
            print = "Buzz";
        } else {
            print += "Buzz";
        }
    } 
    if (print == undefined) {
        print = i;
    }
    console.log(print);
}