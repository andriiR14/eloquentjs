for (let i = 1; i <= 100; i++) {
    let print;
    if (i%3 == 0) {
        print = "Fizz";
    } else if (i%5 == 0) {
        print = "Buzz";
    } 
    if (print == undefined) {
        print = i;
    }
    console.log(print);
}