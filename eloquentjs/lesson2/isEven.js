function isEven(num) {
    if (num > 1)
        return isEven(num - 2);
    else
        return !+num;
}

console.log(isEven(Math.abs(process.argv[2])));