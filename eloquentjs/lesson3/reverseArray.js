function reverseArray(ar) {

    let result = [];
    for (let i = 0; i < ar.length; i++) {
        result[i] = ar[ar.length - 1 - i];
    }

    return result;
}

console.log(reverseArray(process.argv.slice(2)));