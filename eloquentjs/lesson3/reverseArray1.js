function reverseArray(ar) {

    let result = [];
    while (ar.length > 0) {
        result.push(ar.pop());
    }

    return result;
}

console.log(reverseArray(process.argv.slice(2)));