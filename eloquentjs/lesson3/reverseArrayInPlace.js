function reverseArrayInPlace(ar) {

    for (let i = 0; i < ar.length / 2; i++) {
        [ar[i], ar[ar.length - 1 - i]] = [ar[ar.length - 1 - i], ar[i]];
    }

    return ar;
}

console.log(reverseArrayInPlace(process.argv.slice(2)));