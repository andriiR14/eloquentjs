function arrayToList(ar) {

    if (!Array.isArray(ar))
        return undefined;

    if (ar.length == 0)
        return null;

    let result = {
        value: parseInt(ar.shift()),
        rest: arrayToList(ar)
    }

    return result;
}

function listToArray(list) {

    if (!list)
        return undefined;

    let result = [];
    if (list.rest)
        result = listToArray(list.rest);
    result.unshift(list.value);

    return result;
}

function prepend(val, list) {

    let result = {
        value: parseInt(val),
        rest: list
    }
    return result;
}

function nth(list, index) {

    index = parseInt(index);
    let result;
    if (index > 0 && list.rest != null) {
        list = list.rest;
        result = nth(list, --index);
    } else if (index == 0)
        result = list.value;

    return result;
}

var list = arrayToList(process.argv.slice(2));
console.log(list);
console.log(prepend(11, list));
console.log(listToArray(list));
console.log(nth(list, 2));
console.log(nth(list, 13));