function deepEqual(obj1, obj2) {

    let result = true;
    if (typeof(obj1) == "object" && typeof(obj2) == "object") {
        for (let p in obj1) {
            result = deepEqual(obj1[p], obj2[p]);
            if (!result)
                break;
        }

    } else {
        result = obj1 == obj2;
    }
    return result;
}

function Person(name, id, work) {
    this.name = name;
    this.id = id;
    this.work = work;
}

function Work(title, salary) {
    this.title = title;
    this.salary = salary;
}

var hr = new Work("HR", 2000);
var person1 = new Person("Max", 12548, new Work("developer", 5000));
var person2 = new Person("Max", 12548, new Work("developer", 5000));
var person3 = new Person("Alex", 25487, new Work("QA", 3000));
var person4 = new Person("Catherine", 32265, hr);
var person5 = new Person("Catherine", 32265, hr);
var person6 = person5;
var person7 = { name: "Max", id: 12548, work: { title: "developer", salary: 5000 } };
var person8 = { name: "Max", id: 12548, work: { title: "developer", salary: 5000 } };
var person9 = { name: "Max", id: 12548 };



console.log("is person1 equal person2 ?", deepEqual(person1, person2));
console.log("is person4 equal person5 ?", deepEqual(person4, person5));
console.log("is person5 equal person6 ?", deepEqual(person5, person6));
console.log("is person3 equal person6 ?", deepEqual(person3, person6));

console.log("is person7 equal person8 ?", deepEqual(person7, person8));
console.log("is person1 equal person7 ?", deepEqual(person1, person7));
console.log("is person8 equal person9 ?", deepEqual(person8, person9));