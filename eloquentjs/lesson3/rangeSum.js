function range(start, end, step) {

    if ((start > end && step > 0) || (start < end && step < 0)) {
        console.log("incorrect params, please check beggin, end and step of range");
    }

    if (step == 0)
        console.log("Incorrect range step")

    let ar = [];
    for (let i = 0; i <= (end - start) / step; i++) {
        ar[i] = start + step * i;
    }
    return ar;
}

function sum(ar) {

    let sum = 0;
    ar.forEach(element => {
        sum += element;
    });
    return sum;
}

console.log(sum(range(+process.argv[2], +process.argv[3], +process.argv[4])));